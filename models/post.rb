class Post < Sequel::Model
  many_to_one :acount

  plugin :validation_helpers
  def validate
    super
    validate_presence([:title, :body])
  end
end
