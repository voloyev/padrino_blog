Sequel.migration do
  up do
    alter_table(:posts) do
      set_column_default :created_at, Sequel::CURRENT_TIMESTAMP
    end
  end

  down do
  end
end
