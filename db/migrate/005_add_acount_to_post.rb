Sequel.migration do
  up do
    alter_table :posts do
      add_column :acount_id, Integer
    end
  end

  down do
    alter_table :posts do
      drop_column :acount_id
    end
  end
end
