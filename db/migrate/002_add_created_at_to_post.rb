Sequel.migration do
  up do
    alter_table(:posts) do
      add_column :created_at, DateTime, null: false
    end
  end

  down do
    alter_table(:posts) do
      delete_column :created_at
    end
  end
end
