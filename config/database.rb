Sequel::Model.raise_on_save_failure = false # Do not throw exceptions on failure
Sequel::Model.db = case Padrino.env
  when :development then Sequel.connect('postgresql://postgres:1111@localhost/blog_padrino_development', :loggers => [logger])
  when :production  then Sequel.connect('postgresql://postgres:1111@localhost/blog_padrino_production',  :loggers => [logger])
  when :test        then Sequel.connect('postgresql://postgres:1111@localhost/blog_padrino_test',        :loggers => [logger])
end
